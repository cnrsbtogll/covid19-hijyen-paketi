import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:toast/toast.dart';

final firestore = FirebaseFirestore.instance;

class NeighborhoodDetail extends StatefulWidget {
  final String district, neighborhood;
  final bool editable;
  NeighborhoodDetail({this.district, this.neighborhood, this.editable = true});
  @override
  _NeighborhoodDetailState createState() => _NeighborhoodDetailState();
}

class _NeighborhoodDetailState extends State<NeighborhoodDetail> {
  int givenPack = 0;
  bool loading = false;

  void changedText(String e) {
    setState(() {
      givenPack = int.parse(e);
    });
  }

  Future<void> decreaseItemsFromStock() async {
    DocumentReference stockRef = firestore.collection("malzemeler").doc("stok");
    await stockRef.update({
      "sayı": FieldValue.increment(-givenPack),
    });
  }

  void updateData(String refPath) async {
    setState(() {
      loading = true;
    });
    DocumentReference ref = firestore.collection("mahalleler").doc(refPath);
    DocumentSnapshot snap = await ref.get();
    int given = snap.data()["dağıtılanPaket"];
    int toGive = snap.data()["dağıtılacakPaket"];
    if (given + givenPack > toGive) {
      Toast.show(
        "Verilen paket ile verilecek toplam paket sayısı aşıldı",
        context,
        backgroundRadius: 8,
        backgroundColor: Colors.red,
        duration: Toast.LENGTH_LONG,
      );
      setState(() {
        loading = false;
      });
      return;
    } else {
      bool completed = false;
      if (given + givenPack == toGive) {
        completed = true;
      }
      await ref.update({
        "dağıtılanPaket": given + givenPack,
        "tamamlandı": completed,
      });
      await decreaseItemsFromStock();
      Toast.show(
        completed
            ? "Bu mahalle için dağıtım tamamlandı"
            : "Verilen paket güncellendi",
        context,
        backgroundRadius: 8,
        backgroundColor: Colors.green,
        duration: Toast.LENGTH_LONG,
      );
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String refPath = widget.district + "_" + widget.neighborhood;
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.chevron_left, color: Colors.black),
                  onPressed: Navigator.of(context).pop,
                ),
              ),
              Image.asset(
                "assets/sakarya_bb.png",
                width: size.width * .3,
                height: size.width * .3,
              ),
              SizedBox(height: 48),
              Text(
                widget.district + " - " + widget.neighborhood,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  letterSpacing: 1,
                ),
              ),
              SizedBox(height: 16),
              NeigborhoodInfos(
                ref: widget.district + "_" + widget.neighborhood,
              ),
              SizedBox(height: 16),
              if (widget.editable)
                TextField(
                  onChanged: changedText,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Verilen paket",
                  ),
                ),
              SizedBox(height: 32),
              InkWell(
                onTap: () => widget.editable
                    ? loading ? null : updateData(refPath)
                    : null,
                child: Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: widget.editable
                        ? loading ? Colors.grey : Colors.green
                        : Colors.black45,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    widget.editable
                        ? loading ? "Güncelleniyor" : "Güncelle"
                        : "Dağıtım Tamamlandı",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 2,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NeigborhoodInfos extends StatelessWidget {
  final String ref;
  NeigborhoodInfos({this.ref});
  @override
  Widget build(BuildContext context) {
    DocumentReference queryRef = firestore.collection("mahalleler").doc(ref);
    Stream<DocumentSnapshot> stream = queryRef.snapshots();
    return StreamBuilder<DocumentSnapshot>(
      stream: stream,
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());
        Map<String, dynamic> data = snapshot.data.data();
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Dağıtılacak paket",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
            ),
            Text(data["dağıtılacakPaket"].toString()),
            Divider(height: 32, thickness: 1, color: Colors.grey),
            Text(
              "Dağıtılan paket",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
            ),
            Text(data["dağıtılanPaket"].toString()),
            Divider(height: 32, thickness: 1, color: Colors.grey),
          ],
        );
      },
    );
  }
}
