import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:toast/toast.dart';

final firestore = FirebaseFirestore.instance;

class ProductDetail extends StatefulWidget {
  final String name, id;
  ProductDetail({this.name, this.id});
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  int count = 0;
  bool loading = false;

  void changedText(String e) {
    setState(() {
      count = int.parse(e);
    });
  }

  void updateData(String refPath, {bool increase = true}) async {
    setState(() {
      loading = true;
    });
    DocumentReference ref = firestore.collection("malzemeler").doc(refPath);
    DocumentSnapshot snap = await ref.get();
    int remaining = snap.data()["sayı"];
    if (!increase && count > remaining) {
      Toast.show(
        "Çıkarılacak miktar kadar stok bulunmuyor",
        context,
        backgroundRadius: 8,
        backgroundColor: Colors.red,
        duration: Toast.LENGTH_LONG,
      );
      setState(() {
        loading = false;
      });
      return;
    } else {
      int toAdd = increase ? count : -count;
      await ref.update({
        "sayı": FieldValue.increment(toAdd),
      });
      Toast.show(
        "Stok güncellendi",
        context,
        backgroundRadius: 8,
        backgroundColor: Colors.green,
        duration: Toast.LENGTH_LONG,
      );
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String refPath = widget.id;
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.chevron_left, color: Colors.black),
                  onPressed: Navigator.of(context).pop,
                ),
              ),
              Image.asset(
                "assets/sakarya_bb.png",
                width: size.width * .3,
                height: size.width * .3,
              ),
              SizedBox(height: 48),
              Text(
                widget.name,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  letterSpacing: 1,
                ),
              ),
              SizedBox(height: 16),
              ProductInfo(
                ref: refPath,
              ),
              SizedBox(height: 16),
              TextField(
                onChanged: changedText,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Stok Değişimi",
                ),
              ),
              SizedBox(height: 32),
              InkWell(
                onTap: () =>
                    loading ? null : updateData(refPath, increase: false),
                child: Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: loading ? Colors.grey : Colors.red,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    loading ? "Güncelleniyor" : "Azalt",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 2,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16),
              InkWell(
                onTap: () => loading ? null : updateData(refPath),
                child: Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: loading ? Colors.grey : Colors.green,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    loading ? "Güncelleniyor" : "Arttır",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 2,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProductInfo extends StatelessWidget {
  final String ref;
  ProductInfo({this.ref});
  @override
  Widget build(BuildContext context) {
    DocumentReference queryRef = firestore.collection("malzemeler").doc(ref);
    Stream<DocumentSnapshot> stream = queryRef.snapshots();
    return StreamBuilder<DocumentSnapshot>(
      stream: stream,
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());
        Map<String, dynamic> data = snapshot.data.data();
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Kalan Adet",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
            ),
            Text(data["sayı"].toString() + " " + data["birim"]),
            Divider(height: 32, thickness: 1, color: Colors.grey),
          ],
        );
      },
    );
  }
}
