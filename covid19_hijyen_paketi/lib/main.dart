import 'package:covid19_hijyen_paketi/admin_home.dart';
import 'package:covid19_hijyen_paketi/completed_neighborhood.dart';
import 'package:covid19_hijyen_paketi/distribution_dates.dart';
import 'package:covid19_hijyen_paketi/distribution_plan.dart';
import 'package:covid19_hijyen_paketi/employee_list.dart';

import 'package:covid19_hijyen_paketi/landing.dart';
import 'package:covid19_hijyen_paketi/product_list.dart';
import 'package:covid19_hijyen_paketi/stock.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        '/': (context) => LandingPage(),
        '/admin': (context) => AdminHome(),
        '/distribution': (context) => DistributionPlan(),
        '/productlist': (context) => ProductList(),
        '/stock': (context) => StockDetail(),
        '/completed': (context) => CompletedNeighborhood(),
        '/dates': (context) => DistributionDates(),
        '/employees': (context) => EmployeeList(),
      },
    );
  }
}
