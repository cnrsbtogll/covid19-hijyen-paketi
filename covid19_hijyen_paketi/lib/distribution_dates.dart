import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:expandable/expandable.dart';

class DistributionDates extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.chevron_left, color: Colors.black),
                  onPressed: Navigator.of(context).pop,
                ),
              ),
              Image.asset(
                "assets/sakarya_bb.png",
                width: size.width * .3,
                height: size.width * .3,
              ),
              SizedBox(height: 16),
              Text(
                "Dağıtım Tarihleri",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  letterSpacing: 1,
                ),
              ),
              Expanded(
                child: ListView(
                  children: [
                    ...dagitimTarihleri.keys
                        .map((e) => renderExpandableDistrict(e))
                        .toList(),
                    SizedBox(height: 48),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget renderExpandableDistrict(String districtName) {
  ExpandableController _controller = ExpandableController();
  return ExpandableNotifier(
    controller: _controller,
    child: Expandable(
      collapsed: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(districtName),
              IconButton(
                icon: Icon(Icons.arrow_downward),
                onPressed: () => _controller.toggle(),
              ),
            ],
          ),
          Divider(height: 16, thickness: 1, color: Colors.grey),
        ],
      ),
      expanded: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(districtName),
              IconButton(
                icon: Icon(Icons.arrow_upward),
                onPressed: () => _controller.toggle(),
              ),
            ],
          ),
          ...dagitimTarihleri[districtName]
              .keys
              .map((e) => Column(
                    children: [
                      renderNeighborhoods(districtName, e),
                      Divider(height: 16, thickness: 1, color: Colors.grey),
                    ],
                  ))
              .toList(),
        ],
      ),
    ),
  );
}

Widget renderNeighborhoods(String districtName, String date) {
  ExpandableController _controller = ExpandableController();
  return ExpandableNotifier(
    controller: _controller,
    child: Expandable(
      collapsed: ExpandableButton(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(date),
      )),
      expanded: ExpandableButton(
        child: SingleChildScrollView(
          child: Column(
              children: dagitimTarihleri[districtName][date]
                  .map((e) => Text(e))
                  .toList()),
        ),
      ),
    ),
  );
}
