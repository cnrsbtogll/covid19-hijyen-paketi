import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:toast/toast.dart';

final firestore = FirebaseFirestore.instance;

class EmployeeDetail extends StatefulWidget {
  final String employeeId;
  EmployeeDetail({this.employeeId});
  @override
  _EmployeeDetailState createState() => _EmployeeDetailState();
}

class _EmployeeDetailState extends State<EmployeeDetail> {
  int count = 0;
  bool loading = false;

  void changedText(String e) {
    setState(() {
      count = int.parse(e);
    });
  }

  void updateData() async {
    setState(() {
      loading = true;
    });
    DocumentReference ref =
        firestore.collection("personeller").doc(widget.employeeId);
    await ref.update({
      "maas": count,
    });
    Toast.show(
      "Maaş güncellendi",
      context,
      backgroundRadius: 8,
      backgroundColor: Colors.green,
      duration: Toast.LENGTH_LONG,
    );
    setState(() {
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    DocumentReference queryRef =
        firestore.collection("personeller").doc(widget.employeeId);
    Stream<DocumentSnapshot> stream = queryRef.snapshots();
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.chevron_left, color: Colors.black),
                  onPressed: Navigator.of(context).pop,
                ),
              ),
              Image.asset(
                "assets/sakarya_bb.png",
                width: size.width * .3,
                height: size.width * .3,
              ),
              SizedBox(height: 48),
              Text(
                "Personel Detayı",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  letterSpacing: 1,
                ),
              ),
              SizedBox(height: 16),
              SizedBox(height: 16),
              StreamBuilder<DocumentSnapshot>(
                stream: stream,
                builder: (BuildContext context,
                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                  if (!snapshot.hasData)
                    return Center(child: CircularProgressIndicator());
                  Map<String, dynamic> data = snapshot.data.data();
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data["isim"],
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          letterSpacing: 1,
                        ),
                      ),
                      SizedBox(height: 8),
                      Text(
                        "Birim",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      Text(data["birim"]),
                      Text(
                        "Maaş",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      Text(data["maas"].toString() + " ₺"),
                      Divider(height: 32, thickness: 1, color: Colors.grey),
                    ],
                  );
                },
              ),
              SizedBox(height: 16),
              TextField(
                onChanged: changedText,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Maaş Güncelle",
                ),
              ),
              SizedBox(height: 32),
              InkWell(
                onTap: () => loading ? null : updateData(),
                child: Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: loading ? Colors.grey : Colors.green,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    loading ? "Güncelleniyor" : "Maaş Güncelle",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 2,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
