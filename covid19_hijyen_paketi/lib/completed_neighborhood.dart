import 'package:covid19_hijyen_paketi/neighborhood_detail.dart';
import 'package:covid19_hijyen_paketi/product_detail.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final firestore = FirebaseFirestore.instance;

class CompletedNeighborhood extends StatefulWidget {
  @override
  _CompletedNeighborhoodState createState() => _CompletedNeighborhoodState();
}

class _CompletedNeighborhoodState extends State<CompletedNeighborhood> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.chevron_left, color: Colors.black),
                  onPressed: Navigator.of(context).pop,
                ),
              ),
              Image.asset(
                "assets/sakarya_bb.png",
                width: size.width * .3,
                height: size.width * .3,
              ),
              SizedBox(height: 16),
              Text(
                "Dağıtımı Tamamlanan Mahalleler",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  letterSpacing: 1,
                ),
              ),
              SizedBox(height: 32),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      "İlçe",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        letterSpacing: 2,
                        color: Colors.blue[900],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "Mahalle",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        letterSpacing: 2,
                        color: Colors.blue[900],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              Expanded(child: CompletedNeighborhoodStream()),
            ],
          ),
        ),
      ),
    );
  }
}

class CompletedNeighborhoodStream extends StatelessWidget {
  void navigateDetail(
      BuildContext context, String district, String neighborhood) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => NeighborhoodDetail(
          district: district,
          neighborhood: neighborhood,
          editable: false,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Query queryRef =
        firestore.collection("mahalleler").where("tamamlandı", isEqualTo: true);
    Stream<QuerySnapshot> stream = queryRef.snapshots();
    return StreamBuilder<QuerySnapshot>(
      stream: stream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());
        return ListView(
          children: snapshot.data.docs.map((e) {
            List<String> idList = e.id.split("_");
            return InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () => navigateDetail(context, idList[0], idList[1]),
              child: Column(
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(flex: 1, child: Text(idList[0])),
                      Expanded(flex: 1, child: Text(idList[1])),
                    ],
                  ),
                  Divider(height: 32, color: Colors.grey, thickness: 1),
                ],
              ),
            );
          }).toList(),
        );
      },
    );
  }
}
