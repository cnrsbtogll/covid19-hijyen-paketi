const adapazari =
    "SALMANLI,ABALI,AŞIRLAR,BİLECİLER,BUDAKLAR,BÜYÜKHATAPLI,ÇAĞLAYAN,DEMİRBEY,ELMALI,HACILAR,IŞIKLAR,İLYASLAR,KARADAVUTLU,KARADERE,KAVAKLIORMAN,KAYRANCIK,KIŞLA,KÖMÜRLÜK,KÖPRÜBAŞI,KÜÇÜKHATAPLI,NASUHLAR,ÖRENTEPE,POYRAZLAR,SOLAKLAR,TURNADERE,YEŞİLYURT,KARAKÖY,KASIMLAR,KURTBEYLER,MERKEZ,ÇERÇİLER,TAŞLIK,DOĞANCILAR,YENİMAHALLE,HACIRAMAZANLAR,RÜSTEMLER,ÇELEBİLER,ÇÖKEKLER,AKINCILAR,ALANDÜZÜ,BAĞLAR,15 TEMMUZ CAMİLİ,CUMHURİYET,ÇALTICAK,ÇAMYOLU,ÇUKURAHMEDİYE,DAĞDİBİ,GÖKTEPE,GÜLLÜK,HARMANTEPE,HIZIRTEPE,İKİZCE OSMANİYE,İSTİKLAL,KARAKAMIŞ,KARAMAN,KARAOSMAN,KARAPINAR,KORUCUK,KURTULUŞ,MAHMUDİYE,MALTEPE,MİTHATPAŞA,OZANLAR,PABUÇÇULAR,SAKARYA,SEMERCİLER,SÜLEYMANBEY,ŞEKER,ŞİRİNEVLER,TAŞKISIĞI,TEKELER,TEPEKUM,TIĞCILAR,TUZLA,YAĞCILAR,YAHYALAR,YENİCAMİ,YENİDOĞAN,YENİGÜN,ACIELMALI,EVREN,İKİZCE MÜSLÜM,ORTA,BAYRAKTAR";

const akyazi =
    "DOKURCUN,ALAAĞAÇ,BALLIKAYA,BEDİLTAHİRBEY,BELDİBİ,BIÇKIDERE,BOZTEPE,BUĞDAYLI,ÇATALKÖPRÜ,DÜZYAZI,GÜZLEK,HANYATAK,HARUNUSTA,HASANBEY,KARAÇALILIK,KEPEKLİ,KIZILCIKORMAN,KUMKÖPRÜ,OSMANBEY,PAZARKÖY,REŞADİYE,SUKENARI,ŞEREFİYE,TAŞAĞIL,TAŞBURUN,TAŞYATAK,TOPAĞAÇ,TÜRKORMANKÖY,YAĞCILAR,YENİDOĞAN,YONGALIK,YUVALIK,ÇAKIROĞLU,DURMUŞLAR,GÖKÇELER,GÜVENÇLER,HAYDARLAR,MADENLER,MANSURLAR,SALİHİYE,DEDELER,YÖRÜKYERİ,BATAKKÖY,BEDİLKADİRBEY,CUMHURİYET,ÇILDIRLAR,ERDOĞDU,ESKİBEDİL,FATİH,G. SÜLEYMANPAŞA,GEBEŞ,HASTANE,İNÖNÜ,KABAKULAK,BEDİL KAZANCI,KONURALP,ÖMERCİKLER,SEYFELER,UZUNÇINAR,VAKIF,YAHYALI,YENİ,YENİORMAN,YUNUSEMRE,AKBALIK,KÜÇÜCEK İSTİKLAL,CUMHURİYET (KÜÇÜCEK),MERKEZ YENİKÖY,ALTINDERE GÜNDOĞAN,ALTINDERE CUMHURİYET,ALTINDERE OSMANAĞA,KUZULUK ORTAMAHALLE,KUZULUK TOPÇUSIRTI";

const arifiye =
    "BOĞAZKÖY,KIŞLAÇAY,AHMEDİYE,ADLİYE,KEMALİYE,YUKARIKİRAZCA,ÇINARDİBİ,ARİF BEY,AŞAĞI KİRAZCA,HANLI SAKARYA,MOLLAKÖY,CUMHURİYET,KUMBAŞI,KİRAZCA,NEVİYE,FATİH,HANLI MERKEZ,HACIKÖY,ÇAYBAŞIFUADİYE,TÜRKÇAYBAŞI,HANLIKÖY,KARAAPTİLER,SEMERCİLER,KARAÇOMAKLAR";

const erenler =
    "DEĞİRMENDERE,EKİNLİ,KAYALARMEMDUHİYE,KAYALARREŞİTBEY,KÜÇÜKESENCE,YAZILI,BAĞLAR,HACIOĞLU,EPÇELLER,HÜRRİYET,ŞÜKRİYE,HASANBEY,PİRAHMATLER,ŞEYHKÖY,ÇAYBAŞIYENİKÖY,ERENLER,TEPE,NAKIŞLAR,YENİMAHALLE,KAMIŞLI,KOZLUK,DİLMEN,BEKİRPAŞA,EMİRLER,BÜYÜKESENCE,KÜPÇÜLER,SARICALAR,TABAKHANE,ÇAYKIŞLA,YEŞİLTEPE,TUAPSALAR,ALANCUMA,HOROZLAR";

const ferizli =
    "AĞACIK,AKÇUKUR,BAKIRLI,CEYLANDERE,OSMANAĞA ÇİFTLİĞİ,DOĞANCI,KARADİKEN,ABDÜRREZZAK,KUZCA,NALKÖY,SARIAHMETLER,SEYİFLER,TEBERİK,TOKAT,GÖLKENT,SİNANOĞLU,HOCAOĞLU,İNÖNÜ,KEMALPAŞA,YENİ,DAMLIK,DEĞİRMENCİK,İSTİKLAL,DEVLET";

const geyve =
    "AHİBABA,AKDOĞAN,AKKAYA,ALIPLAR,BAĞCAZ,BAĞLARBAŞI,BAYAT,BELPINAR,BOZÖREN,BURHANİYE,CECELER,ÇAMLIK,ÇENGEL,ÇİNE,ÇUKURKÖY,DEMİRLER,DEREKÖY,DOĞANTEPE,DÜZAKÇAŞEHİR,EPÇELER,ESENKÖY,EŞME,GÜNEY,GÜNHOŞLAR,HACILAR,HACIOSMANLAR,HALİDİYE,HIRKA,HİSARLIK,ILICAKÖY,İHSANİYE,KAMIŞLI,KARACAÖREN,KORU,KOZAN,KÖPRÜBAŞI,KULFALLAR,MELEKŞEORUÇ,MELEKŞESOLAK,SAFİBEY,SARAÇLI,SARIGAZİ,SEKİHARMAN,SETÇE,SUÇATI,SÜTALAN,ŞEHREN,TAŞOLUK,UMURBEY,YAYLAK,DOĞANÇAY,AKINCI,FINDIKSUYU,İLİMBEY,KARAÇAM,KIZILKAYA,MAKSUDİYE,NURUOSMANİYE,ÖRENCİK,ŞEREFİYE,ÇAYKÖY,DOĞANCIL,POYDULAR,SABIRLAR,ALİFUATPAŞA,CAMİKEBİR,GAZİSÜLEYMAN PAŞA,ORHANİYE,TEPECİKLER,İNCİKSUYU,YÖRÜKLER,AYDINLAR,ÇELTİKLER";

const hendek =
    "KARADERE,AKARCA,AKÇAYIR,AKTEFEK,AŞAĞIÇALICA,BALIKLIİHSANİYE,BEYKÖY,BEYLİCE,BIÇKIATİK,ÇAKALLIK,ÇAYIRBAŞI,ÇOBANYATAK,ÇUKURHAN,DEREKÖY,ESENTEPE,ESKİBIÇKI,GÜLDİBİ,GÜNDOĞAN,HACIKIŞLA,HACIMBEY,HALLAÇ,HAMİTLİ,HARMANTEPE,HİCRİYE,HÜSEYİNŞEYH,İKBALİYE,İKRAMİYE,KALAYIK,KAHRAMAN,KARATOPRAK,KARGALIYENİKÖY,KAZIMİYE,KIRKTEPE,KOCATÖNGEL,LÜTFİYEKÖŞK,MARTİNLER,MURADİYE,NURİYE,PAŞAKÖY,PINARLI,SARIYER,SERVETİYE,SİVRİTEPE,SOFULAR,SOĞUKSU,SUKENARI,SÜLEYMANİYE,ŞEYHLER,TUZAK,TÜRBE,UZUNCAORMAN,YAĞBASAN,YARICA,YAYALAR,YENİKÖY,YEŞİLKÖY,YUKARIÇALICA,YUKARIHÜSEYİNŞEYH,AKSU,BAKACAK,DİKMEN,GÜNEY,KADİFEKALE,KIZANLIK,KURTULUŞ,SÜMBÜLLÜ,YENİYAYLA,ÇAMLICA,YEŞİLVADİ,AKPINAR,BAŞPINAR,BÜYÜKDERE,ÇİFTLİK,DEREBOĞAZI,KARGALI HANBABA,KEMALİYE,KÖPRÜBAŞI,MAHMUTBEY,NECATİPAŞA,RASİMPAŞA,TURANLAR,YEŞİLYURT,YENİ,AKOVA,KOCAAHMETLER,UZUNÇARŞI,SARIDEDE,BAYRAKTEPE,PUNA ORTAKÖY,GÖKSU,KURTKÖY";

const karapurcek =
    "AHMEDİYE,KIZILBAYIR,KANLIÇAY,KÜÇÜKKARAPÜRÇEK,MECİDİYE,MESUDİYE,ÇEŞMEBAŞI,AHMETLER,CUMHURİYET,HOCAKÖY,İNÖNÜ,YAZILIGÜRGEN,YÜKSEL,MEŞEPINARI";

const karasu =
    "ADATEPE,AKKUM,ARDIÇBELİ,CAMİTEPE,ÇATALÖVEZ,DENİZKÖY,GÖLKÖPRÜ,HÜRRİYET,İHSANİYE,KANCALAR,KARAMÜEZZİNLER,KARANLIKDERE,KARAPINAR,KIZILCIK,KONACIK,KURUMEŞE,KUYUMCULLU,MANAVPINARI,ORTAKÖY,PARALI,RESULLER,SUBATAĞI,TAŞLIGEÇİT,TEPETARLA,TUZLA,ÜÇOLUK,YASSIGEÇİT,YENİDOĞAN,YEŞİLKÖY,DARIÇAYIRI,KURUDERE,LİMANDERE,YUVALIDERE,AZİZİYE,İNCİLLİ,KABAKOZ,KUZULUK,YENİ,YALI";

const kaynarca =
    "AKBAŞLI,ARİFAĞA,BAŞOĞLU,BİRLİK,BÜYÜKYANIK,CEBEK,EĞRİOĞLU,GAZİLER,GÖLCE,GÜRPINAR,GÜVEN,İMAMLAR,İŞARET,KALBURCU,KARAÇALI,KARAMANLAR,KAYACIK,KERTİL,KIRKTEPE,KIZILCAALİ,KULAKLI,KÜÇÜKKAYNARCA,KÜÇÜKKIŞLA,MÜEZZİNLER,OKÇULAR,ORTAKÖY,OSMANLI,ÖMERAĞA,SABIRLI,SARIBEYLİ,ŞEYHTIMARI,TAŞOLUK,TOPÇU,TURNALI,UĞURLU,UZUNALAN,YENİÇAM,YEŞİLOVA,ZİAHMET,UZAKKIŞLA,HATİPLER,KONAK,DUDU,MERKEZ MAHALLE,ORTA MAHALLE";

const kocaali =
    "AÇMABAŞI,AKPINAR,BEZİRGAN,CAFERİYE,ÇAKMAKLI,ÇUKURKÖY,DEMİRAÇMA,KADIKÖY,KARAPELİT,KESTANEPINARI,KİRAZLI,KOĞUTPELİT,KOZLUK,MELEN,SELAHİYE,ŞERBETPINAR,AKTAŞ,AYDOĞAN,BEYLER,KARALAR,KÖYYERİ,SÜNGÜT,YALPANKAYA,YANIKSAYVANT,GÜMÜŞOLUK ,KIZILÜZÜM,GÖRELE,AĞALAR,ALANDERE,YENİ,HIZAR,KARŞI,YAYLA,MERKEZ,KÜPLÜK,YALI";

const pamukova =
    "AĞAÇÇILAR,AHILAR,AKÇAKAYA,BACIKÖY,BAKACAK,BAYIRAKÇAŞEHİR,CİHADİYE,ÇARDAK,ÇİLEKLİ,EĞRİÇAY,ESKİYAYLA,FEVZİYE,GÖKGÖZ,HAYRETTİN,HÜSEYİNLİ,İSABALI,KADIKÖY,KARAPINAR,KAZIMİYE,KEMALİYE,MEKECE,GONCA,ORUÇLU,ÖZBEK,PAŞALAR,PINARLI,ŞAHMELEK,ŞEYHVARMAZ,TEŞVİKİYE,TURGUTLU,YENİCE,CUMHURİYET,ELPEREK";

const sapanca =
    "AKÇAY,BALKAYA,FEVZİYE,HACIMERCAN,İKRAMİYE,İLMİYE,KURUÇEŞME,MAHMUDİYE,MEMNUNİYE,MURADİYE,ŞÜKRİYE,UZUNKUM,ÜNLÜCE,YANIK,İSTANBULDERE,CAMİCEDİT,ÇAYİÇİ,GAZİPAŞA,GÖL,GÜLDİBİ,RÜSTEMPAŞA,YENİMAHALLE,KURTKÖY YAVUZSELİM,KIRKPINAR TEPEBAŞI,KURTKÖY FATİH,KIRKPINAR HASANPAŞA,KIRKPINAR SOĞUKSU,KURTKÖY DİBEKTAŞ,NAİLİYE";

const serdivan =
    "AŞAĞIDEREKÖY,BEŞEVLER,ÇUBUKLU,DAĞYONCALI,KIZILCIKLI,KURUÇEŞME,MEŞELİ,SELAHİYE,YUKARIDEREKÖY,UZUNKÖY,ARALIK,HAMİTABAT,İSTİKLAL,KÖPRÜBAŞI,KEMALPAŞA,32 EVLER,ARABACIALANI,ESENTEPE,REŞADİYE,KAZIMPAŞA,ORTA,BAHÇELİEVLER,VATAN,BEŞKÖPRÜ";

const sogutlu =
    "BEŞDEĞİRMEN,FINDIKLI,HASANFAKI,İMAMLAR,KANTAR,KARATEKE,KURUDİL,LEVENT,MAĞARA,MAKSUDİYE,SIRAKÖY,SOĞUCAK,TOKMAKLIDERE,TÜRKBEYLİKKIŞLA,YENİKÖY,CAMİCEDİD,GÜNDOĞAN,KÜÇÜKSÖĞÜTLÜ,ORTA,AKARCA,AKÇAKAMIŞ,AKGÖL,RÜSTEMLER";

const tarakli =
    "AKÇAPINAR,AKSU,ALBALLAR,AVDAN,ÇAMTEPE,DIŞDEDELER,DUMAN,ESENYURT,HACIALİLER,HACIYAKUP,HARKKÖY,İÇDEDELER,KEMALLER,MAHDUMLAR,PİRLER,TUZLA,UĞURLU,YENİKÖY,HACIMURAT,YUSUFBEY,ULUCAMİ,YENİDOĞAN";

const ilceListesi = [
  "ADAPAZARI",
  "AKYAZI",
  "ARİFİYE",
  "ERENLER",
  "FERİZLİ",
  "GEYVE",
  "HENDEK",
  "KARAPÜRÇEK",
  "KARASU",
  "KAYNARCA",
  "KOCAALİ",
  "PAMUKOVA",
  "SAPANCA",
  "SERDİVAN",
  "SÖĞÜTLÜ",
  "TARAKLI",
];

final Map<String, List<String>> mahalleListesi = {
  "ADAPAZARI": adapazari.split(","),
  "AKYAZI": akyazi.split(","),
  "ARİFİYE": arifiye.split(","),
  "ERENLER": erenler.split(","),
  "FERİZLİ": ferizli.split(","),
  "GEYVE": geyve.split(","),
  "HENDEK": hendek.split(","),
  "KARAPÜRÇEK": karapurcek.split(","),
  "KARASU": karasu.split(","),
  "KAYNARCA": kaynarca.split(","),
  "KOCAALİ": kocaali.split(","),
  "PAMUKOVA": pamukova.split(","),
  "SAPANCA": sapanca.split(","),
  "SERDİVAN": serdivan.split(","),
  "SÖĞÜTLÜ": sogutlu.split(","),
  "TARAKLI": tarakli.split(","),
};

const dagitimTarihleri = {
  "Adapazarı": {
    "01.04.2020": [
      "15 TEMMUZ CAMİLİ MAHALLESİ",
      "ABALI MAHALLES",
      "ACIELMALIK MAHALLES",
      "AKINCILAR MAHALLES",
      "ALANDÜZÜ MAHALLES",
      "AŞIRLAR MAHALLES",
      "BAĞLAR MAHALLES",
      "BAYRAKTAR MAHALLES",
      "BİLECİLER MAHALLES",
      "BUDAKLAR MAHALLES",
      "BÜYÜKHATAPLI MAHALLESİ"
    ],
    "02.04.2020": [
      "CUMHURİYET MAHALLESİ",
      "ÇAĞLAYAN MAHALLESİ",
      "ÇALTICAK MAHALLESİ",
      "ÇAMYOLU MAHALLESİ",
      "ÇELEBİLER MAHALLESİ",
      "ÇERÇİLER MAHALLESİ",
      "ÇÖKEKLER MAHALLESİ",
      "ÇUKURAHMEDİYE MAHALLESİ",
      "DAĞDİBİ MAHALLESİ",
      "DEMİRBEY MAHALLESİ",
      "DOĞANCILAR MAHALLESİ",
      "ELMALI MAHALLESİ",
      "EVRENKÖY MAHALLESİ",
      "GÖKTEPE MAHALLESİ",
      "GÜLLÜK MAHALLESİ",
      "GÜNEŞLER MERKEZ MAHALLESİ",
      "GÜNEŞLER YENİ MAHALLESİ",
      "HACILAR MAHALLESİ",
      "HACIRAMAZANLAR MAHALLESİ",
      "HARMANTEPE MAHALLESİ",
      "HIZIRTEPE MAHALLESİ",
      "IŞIKLAR MAHALLESİ"
    ],
    "03.04.2020": [
      "İKİZCE MÜSLİM MAHALLESİ",
      "İKİZCE OSMANİYE MAHALLESİ",
      "İLYASLAR MAHALLESİ",
      "İSTİKLAL MAHALLESİ",
      "KARADAVUTLU MAHALLESİ",
      "KARADERE MAHALLESİ",
      "KARAKAMIŞ MAHALLESİ",
      "KARAKÖY MAHALLESİ",
      "KARAMAN MAHALLESİ",
      "KARAOSMAN MAHALLESİ",
      "KARAPINAR MAHALLESİ",
      "KASIMLAR MAHALLESİ",
      "KAVAKLIORMAN MAHALLESİ",
      "KAYRANCIK MAHALLESİ",
      "KIŞLA MAHALLESİ",
      "KORUCUK MAHALLESİ",
      "KÖMÜRLÜK MAHALLESİ",
      "KÖPRÜBAŞI MAHALLESİ",
      "KURTBEYLER MAHALLESİ",
      "KURTULUŞ MAHALLESİ",
      "KÜÇÜKHATAPLI MAHALLESİ",
      "MAHMUDİYE MAHALLESİ"
    ],
    "04.04.2020": [
      "MALTEPE MAHALLESİ",
      "MİTHATPAŞA MAHALLESİ",
      "NASUHLAR MAHALLESİ",
      "ORTA MAHALLESİ",
      "OZANLAR MAHALLESİ",
      "ÖRENTEPE MAHALLESİ",
      "PAPUÇÇULAR MAHALLESİ",
      "POYRAZLAR MAHALLESİ",
      "RÜSTEMLER MAHALLESİ",
      "SAKARYA MAHALLESİ",
      "SALMANLI MAHALLESİ",
      "SEMERCİLER MAHALLESİ",
      "SOLAKLAR MAHALLESİ",
      "SÜLEYMANBEY MAHALLESİ"
    ],
    "05.04.2020": [
      "ŞEKER MAHALLESİ",
      "ŞİRİNEVLER MAHALLESİ",
      "TAŞKISIĞI MAHALLESİ",
      "TAŞLIK MAHALLESİ",
      "TEKELER MAHALLESİ",
      "TEPEKUM MAHALLESİ",
      "TIĞCILAR MAHALLESİ",
      "TURNADERE MAHALLESİ",
      "TUZLA MAHALLESİ",
      "YAĞCILAR MAHALLESİ",
      "YAHYALAR MAHALLESİ",
      "YENİCAMİ MAHALLESİ",
      "YENİDOĞAN MAHALLESİ",
      "YENİGÜN MAHALLESİ",
      "YEŞİLYURT MAHALLESİ"
    ],
  },
  "Akyazı": {
    "06.04.2020": [
      "AKBALIK MAHALLESİ",
      "ALAAĞAÇ MAHALLESİ",
      "ALTINDERE CUMHURİYET MAHALLESİ",
      "ALTINDERE GÜNDOĞAN MAHALLESİ",
      "ALTINDERE OSMANAĞA MAHALLESİ",
      "BALLIKAYA MAHALLESİ",
      "BATAKKÖY MAHALLESİ",
      "BEDİL KAZANCI MAHALLESİ",
      "BEDİLKADIRBEY MAHALLESİ",
      "BEDİLTAHİRBEY MAHALLESİ",
      "BELDİBİ MAHALLESİ",
      "BIÇKIDERE MAHALLESİ",
      "BOZTEPE MAHALLESİ",
      "BUĞDAYLI MAHALLESİ",
      "CUMHURİYET MAHALLESİ"
    ],
    "07.04.2020": [
      "ÇAKIROĞLU MAHALLESİ",
      "ÇATALKÖPRÜ MAHALLESİ",
      "ÇILDIRLAR MAHALLESİ",
      "DEDELER MAHALLESİ",
      "DOKURCUN MAHALLESİ",
      "DURMUŞLAR MAHALLESİ",
      "DÜZYAZI MAHALLESİ",
      "ERDOĞDU MAHALLESİ",
      "ESKİBEDİL MAHALLESİ",
      "FATİH MAHALLESİ",
      "GAZİ SÜLEYMAN PAŞA MAHALLESİ",
      "GEBEŞ MAHALLESİ",
      "GÖKÇELER MAHALLESİ",
      "GÜVENÇLER MAHALLESİ",
      "GÜZLEK MAHALLESİ",
      "HANYATAK MAHALLESİ",
      "HARUNUSTA MAHALLESİ",
      "HASANBEY MAHALLESİ",
      "HASTAHANE MAHALLESİ",
      "HAYDARLAR MAHALLESİ",
      "İNÖNÜ MAHALLESİ",
      "KABAKULAK MAHALLESİ",
      "KARAÇALILIK MAHALLESİ",
      "KEPEKLİ MAHALLESİ",
      "KIZILCIKORMAN MAHALLESİ",
      "KONURALP MAHALLESİ",
      "KUMKÖPRÜ MAHALLESİ",
      "KUZULUK ORTAMAHALLE MAHALLESİ",
      "KUZULUK TOPÇUSIRTI MAHALLESİ",
      "KÜÇÜCEK CUMHURİYET MAHALLESİ",
      "KÜÇÜCEK İSTİKLAL MAHALLESİ",
      "MADENLER MAHALLESİ",
      "MANSURLAR MAHALLESİ",
      "MERKEZYENİKÖY MAHALLESİ",
      "OSMANBEY MAHALLESİ"
    ],
    "08.04.2020": [
      "ÖMERCİKLER MAHALLESİ",
      "PAZARKÖY MAHALLESİ",
      "REŞADİYE MAHALLESİ",
      "SALİHİYE MAHALLESİ",
      "SEYFELER MAHALLESİ",
      "SUKENARI MAHALLESİ",
      "ŞEREFİYE MAHALLESİ",
      "TAŞAĞIL MAHALLESİ",
      "TAŞBURUN MAHALLESİ",
      "TAŞYATAK MAHALLESİ",
      "TOPAĞAÇ MAHALLESİ",
      "TÜRKORMANKÖY MAHALLESİ",
      "UZUNÇINAR MAHALLESİ",
      "VAKIF MAHALLESİ",
      "YAĞCILAR MAHALLESİ",
      "YAHYALI MAHALLESİ",
      "YENİ MAHALLESİ",
      "YENİDOĞAN MAHALLESİ",
      "YENİORMAN MAHALLESİ",
      "YONGALIK MAHALLESİ",
      "YÖRÜKYERİ MAHALLESİ",
      "YUNUS EMRE MAHALLESİ",
      "YUVALAK MAHALLESİ"
    ],
  },
  "Arifiye": {
    "09.04.2020": [
      "ADLİYE MAHALLESİ",
      "AHMEDİYE MAHALLESİ",
      "ARİFBEY MAHALLESİ",
      "AŞAĞI KİRAZCA MAHALLESİ",
      "BOĞAZKÖY MAHALLESİ",
      "CUMHURİYET MAHALLESİ",
      "ÇAYBAŞI FUADİYE MAHALLESİ",
      "ÇINARDİBİ MAHALLESİ",
      "FATİH MAHALLESİ",
      "HACIKÖY MAHALLESİ",
      "HANLI MERKEZ MAHALLESİ",
      "HANLI SAKARYA MAHALLESİ",
      "HANLIKÖY MAHALLESİ",
      "KARAAPTİLER MAHALLESİ",
      "KARAÇOMAKLAR MAHALLESİ"
    ],
    "10.04.2020": [
      "KEMALİYE MAHALLESİ",
      "KIŞLAÇAY MAHALLESİ",
      "KİRAZCA MAHALLESİ",
      "KUMBAŞI MAHALLESİ",
      "MOLLAKÖY MAHALLESİ",
      "NEVİYE MAHALLESİ",
      "SEMERCİLER MAHALLESİ",
      "TÜRKÇAYBAŞI MAHALLESİ",
      "YUKARIKİRAZCA MAHALLESİ"
    ],
  },
  "Erenler": {
    "11.04.2020": [
      "ALANCUMA MAHALLESİ",
      "BAĞLAR MAHALLESİ",
      "BEKİRPAŞA MAHALLESİ",
      "BÜYÜKESENCE MAHALLESİ",
      "ÇAYBAŞIYENİKÖY MAHALLESİ",
      "ÇAYKIŞLA MAHALLESİ",
      "DEĞİRMENDERE MAHALLESİ",
      "DİLMEN MAHALLESİ",
      "EKİNLİ MAHALLESİ",
      "EMİRLER MAHALLESİ",
      "EPÇELLER MAHALLESİ",
      "ERENLER MAHALLESİ",
      "HACIOĞLU MAHALLESİ",
      "HASANBEY MAHALLESİ",
      "HOROZLAR MAHALLESİ",
      "HÜRRİYET MAHALLESİ",
      "KAMIŞLI MAHALLESİ",
      "KAYALARMEMDUHİYE MAHALLESİ",
      "KAYALARREŞİTBEY MAHALLESİ",
      "KOZLUK MAHALLESİ"
    ],
    "12.04.2020": [
      "KÜÇÜKESENCE MAHALLESİ",
      "KÜPÇÜLER MAHALLESİ",
      "NAKIŞLAR MAHALLESİ",
      "PİRAHMETLER MAHALLESİ",
      "SARICALAR MAHALLESİ",
      "ŞEYHKÖY MAHALLESİ",
      "ŞÜKRİYE MAHALLESİ",
      "TABAKHANE MAHALLESİ",
      "TEPE MAHALLESİ",
      "TUAPSALAR MAHALLESİ",
      "YAZILI MAHALLESİ",
      "YENİ MAHALLESİ",
      "YEŞİLTEPE MAHALLESİ"
    ],
  },
  "Ferizli": {
    "13.04.2020": [
      "ABDÜRREZZAK MAHALLESİ",
      "AĞACIK MAHALLESİ",
      "AKÇUKUR MAHALLESİ",
      "BAKIRLI MAHALLESİ",
      "CEYLANDERE MAHALLESİ",
      "DAMLIK MAHALLESİ",
      "DEĞİRMENCİK MAHALLESİ",
      "DEVLET MAHALLESİ",
      "DOĞANCI MAHALLESİ",
      "GÖLKENT MAHALLESİ",
      "HOCAOĞLU MAHALLESİ",
      "İNÖNÜ MAHALLESİ",
      "İSTİKLAL MAHALLESİ",
      "KARADİKEN MAHALLESİ"
    ],
    "14.04.2020": [
      "KEMALPAŞA MAHALLESİ",
      "KUSCA MAHALLESİ",
      "NALKÖY MAHALLESİ",
      "OSMANAĞA ÇİFTLİĞİ MAHALLESİ",
      "SARIAHMETLER MAHALLESİ",
      "SEYİFLER MAHALLESİ",
      "SİNANOĞLU MAHALLESİ",
      "TEBERİK MAHALLESİ",
      "TOKAT MAHALLESİ",
      "YENİ MAHALLESİ"
    ],
  },
  "Geyve": {
    "15.04.2020": [
      "AHİBABA MAHALLESİ",
      "AKDOĞAN MAHALLESİ",
      "AKINCI MAHALLESİ",
      "AKKAYA MAHALLESİ",
      "ALIPLAR MAHALLESİ",
      "ALİFUATPAŞA MAHALLESİ",
      "AYDINLAR MAHALLESİ",
      "BAĞCAZ MAHALLESİ",
      "BAĞLARBAŞI MAHALLESİ",
      "BAYAT MAHALLESİ",
      "BELPINAR MAHALLESİ",
      "BOZÖREN MAHALLESİ",
      "BURHANİYE MAHALLESİ",
      "CAMİKEBİR MAHALLESİ",
      "CECELER MAHALLESİ",
      "ÇAMLIK MAHALLESİ",
      "ÇAYKÖY MAHALLESİ",
      "ÇELTİKLER MAHALLESİ",
      "ÇENGEL MAHALLESİ",
      "ÇİNE MAHALLESİ",
      "ÇUKURKÖY MAHALLESİ",
      "DEMİRLER MAHALLESİ",
      "DEREKÖY MAHALLESİ",
      "DOĞANCIL MAHALLESİ",
      "DOĞANÇAY MAHALLESİ",
      "DOĞANTEPE MAHALLESİ",
      "DÜZAKÇAŞEHİR MAHALLESİ",
      "EPÇELER MAHALLESİ",
      "ESENKÖY MAHALLESİ",
      "EŞME MAHALLESİ",
      "FINDIKSUYU MAHALLESİ",
      "GAZİ SÜLEYMAN PAŞA MAHALLESİ",
      "GÜNEY MAHALLESİ",
      "GÜNHOŞLAR MAHALLESİ",
      "HACILAR MAHALLESİ",
      "HACIOSMANLAR MAHALLESİ",
      "HALİDİYE MAHALLESİ",
      "HIRKA MAHALLESİ",
      "HİSARLIK MAHALLESİ",
      "ILICA MAHALLESİ",
      "İHSANİYE MAHALLESİ",
      "İLİMBEY MAHALLESİ",
      "İNCİKSUYU MAHALLESİ",
      "KAMIŞLI MAHALLESİ",
      "KARACAÖREN MAHALLESİ",
      "KARAÇAM MAHALLESİ",
      "KIZILKAYA MAHALLESİ"
    ],
    "16.04.2020": [
      "KORU MAHALLESİ",
      "KOZAN MAHALLESİ",
      "KÖPRÜBAŞI MAHALLESİ",
      "KULFALLAR MAHALLESİ",
      "MAKSUDİYE MAHALLESİ",
      "MELEKŞEORUÇ MAHALLESİ",
      "MELEKŞESOLAK MAHALLESİ",
      "NURUOSMANİYE MAHALLESİ",
      "ORHANİYE MAHALLESİ",
      "ÖRENCİK MAHALLESİ",
      "POYDULAR MAHALLESİ",
      "SABIRLAR MAHALLESİ",
      "SAFİBEY MAHALLESİ",
      "SARAÇLI MAHALLESİ",
      "SARIGAZİ MAHALLESİ",
      "SEKİHARMAN MAHALLESİ",
      "SETÇE MAHALLESİ",
      "SUÇATI MAHALLESİ",
      "SÜTALAN MAHALLESİ",
      "ŞEHREN MAHALLESİ",
      "ŞEREFİYE MAHALLESİ",
      "TAŞOLUK MAHALLESİ",
      "TEPECİKLER MAHALLESİ",
      "UMURBEY MAHALLESİ",
      "YAYLAK MAHALLESİ",
      "YÖRÜKLER MAHALLESİ"
    ],
  },
  "Hendek": {
    "17.04.2020": [
      "AKARCA MAHALLESİ",
      "AKÇAYIR MAHALLESİ",
      "AKOVA MAHALLESİ",
      "AKPINAR MAHALLESİ",
      "AKSU MAHALLESİ",
      "AKTEFEK MAHALLESİ",
      "AŞAĞIÇALICA MAHALLESİ",
      "BAKACAK MAHALLESİ",
      "BALIKLIİHSANİYE MAHALLESİ",
      "BAŞPINAR MAHALLESİ",
      "BAYRAKTEPE MAHALLESİ",
      "BEYKÖY MAHALLESİ",
      "BEYLİCE MAHALLESİ",
      "BIÇKIATİK MAHALLESİ",
      "BÜYÜKDERE MAHALLESİ"
    ],
    "18.04.2020": [
      "ÇAKALLIK MAHALLESİ",
      "ÇAMLICA MAHALLESİ",
      "ÇAYIRBAŞI MAHALLESİ",
      "ÇİFTLİK MAHALLESİ",
      "ÇOBANYATAK MAHALLESİ",
      "ÇUKURHAN MAHALLESİ",
      "DEREBOĞAZI MAHALLESİ",
      "DEREKÖY MAHALLESİ",
      "DİKMEN MAHALLESİ",
      "ESENTEPE MAHALLESİ",
      "ESKİBIÇKI MAHALLESİ",
      "GÖKSU MAHALLESİ",
      "GÜLDİBİ MAHALLESİ",
      "GÜNDOĞAN MAHALLESİ",
      "GÜNEY MAHALLESİ",
      "HACIKIŞLA MAHALLESİ",
      "HACİMBEY MAHALLESİ",
      "HALAÇ MAHALLESİ",
      "HAMİTLİ MAHALLESİ",
      "HARMANTEPE MAHALLESİ",
      "HİCRİYE MAHALLESİ"
    ],
    "19.04.2020": [
      "HÜSEYİNŞEYH MAHALLESİ",
      "İKBALİYE MAHALLESİ",
      "İKRAMİYE MAHALLESİ",
      "KADİFEKALE MAHALLESİ",
      "KAHRAMAN MAHALLESİ",
      "KALAYIK MAHALLESİ",
      "KARADERE MAHALLESİ",
      "KARATOPRAK MAHALLESİ",
      "KARGALI HANBABA MAHALLESİ",
      "KARGALIYENİKÖY MAHALLESİ",
      "KAZIMİYE MAHALLESİ",
      "KEMALİYE MAHALLESİ",
      "KIRKTEPE MAHALLESİ",
      "KIZANLIK MAHALLESİ"
    ],
    "20.04.2020": [
      "KOCAAHMETLER MAHALLESİ",
      "KOCATÖNGEL MAHALLESİ",
      "KÖPRÜBAŞI MAHALLESİ",
      "KURTKÖY MAHALLESİ",
      "KURTULUŞ MAHALLESİ",
      "LÜTFİYEKÖŞK MAHALLESİ",
      "MAHMUTBEY MAHALLESİ",
      "MARTİNLER MAHALLESİ",
      "MURADİYE MAHALLESİ",
      "NECATİPAŞA MAHALLESİ",
      "NURİYE MAHALLESİ",
      "PAŞAKÖY MAHALLESİ",
      "PINARLI MAHALLESİ",
      "PUNA ORTAKÖY MAHALLESİ",
      "RASİMPAŞA MAHALLESİ",
      "SARIDEDE MAHALLESİ",
      "SARIYER MAHALLESİ",
      "SERVETİYE MAHALLESİ",
      "SİVRİTEPE MAHALLESİ",
      "SOFULAR MAHALLESİ",
      "SOĞUKSU MAHALLESİ",
      "SUKENARI MAHALLESİ",
      "SÜLEYMANİYE MAHALLESİ",
      "SÜMBÜLLÜ MAHALLESİ",
      "ŞEYHLER MAHALLESİ",
      "TURANLAR MAHALLESİ"
    ],
    "21.04.2020": [
      "TUZAK MAHALLESİ",
      "TÜRBE MAHALLESİ",
      "UZUNCAORMAN MAHALLESİ",
      "UZUNÇARŞI MAHALLESİ",
      "YAĞBASAN MAHALLESİ",
      "YARICA MAHALLESİ",
      "YAYALAR MAHALLESİ",
      "YENİ MAHALLESİ",
      "YENİKÖY MAHALLESİ",
      "YENİYAYLA MAHALLESİ",
      "YEŞİLKÖY MAHALLESİ",
      "YEŞİLVADİ MAHALLESİ",
      "YEŞİLYURT MAHALLESİ",
      "YUKARIÇALICA MAHALLESİ",
      "YUKARIHÜSEYİNŞEYH MAHALLESİ"
    ],
  },
  "Karapürçek": {
    "22.04.2020": [
      "AHMEDİYE MAHALLESİ",
      "AHMETLER MAHALLESİ",
      "CUMHURİYET MAHALLESİ",
      "ÇEŞMEBAŞI MAHALLESİ",
      "HOCAKÖY MAHALLESİ",
      "İNÖNÜ MAHALLESİ",
      "KANLIÇAY MAHALLESİ",
      "KIZILBAYIR MAHALLESİ",
      "KÜÇÜKKARAPÜRÇEK MAHALLESİ",
      "MECİDİYE MAHALLESİ",
      "MESUDİYE MAHALLESİ",
      "MEŞEPINARI MAHALLESİ",
      "YAZILIGÜRGEN MAHALLESİ",
      "YÜKSEL MAHALLESİ"
    ],
  },
  "Karasu": {
    "23.04.2020": [
      "ADATEPE MAHALLESİ",
      "AKKUM MAHALLESİ",
      "ARDIÇBELİ MAHALLESİ",
      "AZİZİYE MAHALLESİ",
      "CAMİTEPE MAHALLESİ",
      "ÇATALÖVEZ MAHALLESİ",
      "DARIÇAYIRI MAHALLESİ",
      "DENİZKÖY MAHALLESİ",
      "GÖLKÖPRÜ MAHALLESİ",
      "HÜRRİYET MAHALLESİ",
      "İHSANİYE MAHALLESİ",
      "İNCİLLİ MAHALLESİ",
      "KABAKOZ MAHALLESİ",
      "KANCALAR MAHALLESİ"
    ],
    "24.04.2020": [
      "KARAMÜEZZİNLER MAHALLESİ",
      "KARANLIKDERE MAHALLESİ",
      "KARAPINAR MAHALLESİ",
      "KARASU MAHALLESİ",
      "KIZILCIK MAHALLESİ",
      "KONACIK MAHALLESİ",
      "KURUDERE MAHALLESİ",
      "KURUMEŞE MAHALLESİ",
      "KUYUMCULLU MAHALLESİ",
      "KUZULUK MAHALLESİ",
      "LİMANDERE MAHALLESİ",
      "MANAVPINARI MAHALLESİ",
      "ORTAKÖY MAHALLESİ",
      "PARALI MAHALLESİ",
      "RESULLER MAHALLESİ",
      "SUBATAĞI MAHALLESİ",
      "TAŞLIGEÇİT MAHALLESİ",
      "TEPETARLA MAHALLESİ"
    ],
    "25.04.2020": [
      "TUZLA MAHALLESİ",
      "ÜÇOLUK MAHALLESİ",
      "YALI MAHALLESİ",
      "YASSIGEÇİT MAHALLESİ",
      "YENİ MAHALLESİ",
      "YENİDOĞAN MAHALLESİ",
      "YEŞİLKÖY MAHALLESİ",
      "YUVALIDERE MAHALLESİ"
    ],
  },
  "Kaynarca": {
    "26.04.2020": [
      "AKBAŞLI MAHALLESİ",
      "ARİFAĞA MAHALLESİ",
      "BAŞOĞLU MAHALLESİ",
      "BİRLİK MAHALLESİ",
      "BÜYÜKYANIK MAHALLESİ",
      "CEBEK MAHALLESİ",
      "DUDUKÖY MAHALLESİ",
      "EĞRİOĞLU MAHALLESİ",
      "GAZİLER MAHALLESİ",
      "GÖLCE MAHALLESİ",
      "GÜRPINAR MAHALLESİ",
      "GÜVEN MAHALLESİ",
      "HATİPLER MAHALLESİ",
      "İMAMLAR MAHALLESİ",
      "İŞARET MAHALLESİ",
      "KALBURCU MAHALLESİ",
      "KARAÇALI MAHALLESİ",
      "KARAMANLAR MAHALLESİ",
      "KAYACIK MAHALLESİ",
      "KERTİL MAHALLESİ",
      "KIRKTEPE MAHALLESİ",
      "KIZILCAALİ MAHALLESİ"
    ],
    "27.04.2020": [
      "KONAK MAHALLESİ",
      "KULAKLI MAHALLESİ",
      "KÜÇÜKKAYNARCA MAHALLESİ",
      "KÜÇÜKKIŞLA MAHALLESİ",
      "MERKEZ MAHALLESİ",
      "MÜEZZİNLER MAHALLESİ",
      "OKÇULAR MAHALLESİ",
      "ORTA MAHALLESİ",
      "ORTAKÖY MAHALLESİ",
      "OSMANLI MAHALLESİ",
      "ÖMERAĞA MAHALLESİ",
      "SABIRLI MAHALLESİ",
      "SARIBEYLİ MAHALLESİ",
      "ŞEYHTIMARI MAHALLESİ",
      "TAŞOLUK MAHALLESİ",
      "TOPÇU MAHALLESİ",
      "TURNALI MAHALLESİ",
      "UĞURLU MAHALLESİ",
      "UZAKKIŞLA MAHALLESİ",
      "UZUNALAN MAHALLESİ",
      "YENİÇAM MAHALLESİ",
      "YEŞİLOVA MAHALLESİ",
      "ZİAHMET MAHALLESİ"
    ],
  },
  "Kocaali": {
    "28.04.2020": [
      "AÇMABAŞI MAHALLESİ",
      "AĞALAR MAHALLESİ",
      "AKPINAR MAHALLESİ",
      "AKTAŞ MAHALLESİ",
      "ALANDERE MAHALLESİ",
      "AYDOĞAN MAHALLESİ",
      "BEYLER MAHALLESİ",
      "BEZİRGAN MAHALLESİ",
      "CAFERİYE MAHALLESİ",
      "ÇAKMAKLI MAHALLESİ",
      "ÇUKURKÖY MAHALLESİ",
      "DEMİRAÇMA MAHALLESİ",
      "GÖRELE MAHALLESİ",
      "GÜMÜŞOLUK MAHALLESİ",
      "HIZAR MAHALLESİ",
      "KADIKÖY MAHALLESİ",
      "KARALAR MAHALLESİ",
      "KARAPELİT MAHALLESİ",
      "KARŞI MAHALLESİ"
    ],
    "29.04.2020": [
      "KESTANEPINARI MAHALLESİ",
      "KIZILÜZÜM MAHALLESİ",
      "KİRAZLI MAHALLESİ",
      "KOĞUTPELİT MAHALLESİ",
      "KOZLUK MAHALLESİ",
      "KÖYYERİ MAHALLESİ",
      "KÜPLÜK MAHALLESİ",
      "MELEN MAHALLESİ",
      "MERKEZ MAHALLESİ",
      "SELAHİYE MAHALLESİ",
      "SÜNGÜT MAHALLESİ"
    ],
    "30.04.2020": [
      "ŞERBETPINAR MAHALLESİ",
      "YALI MAHALLESİ",
      "YALPANKAYA MAHALLESİ",
      "YANIKSAYVANT MAHALLESİ",
      "YAYLA MAHALLESİ",
      "YENİ MAHALLESİ"
    ],
  },
  "Pamukova": {
    "01.05.2020": [
      "AĞAÇÇILAR MAHALLESİ",
      "AHILAR MAHALLESİ",
      "AKÇAKAYA MAHALLESİ",
      "BACIKÖY MAHALLESİ",
      "BAKACAK MAHALLESİ",
      "BAYIRAKÇAŞEHİR MAHALLESİ",
      "CİHADİYE MAHALLESİ",
      "CUMHURİYET MAHALLESİ",
      "ÇARDAK MAHALLESİ",
      "ÇİLEKLİ MAHALLESİ",
      "EĞRİÇAY MAHALLESİ"
    ],
    "02.05.2020": [
      "ELPEREK MAHALLESİ",
      "ESKİYAYLA MAHALLESİ",
      "FEVZİYE MAHALLESİ",
      "GONCA MAHALLESİ",
      "GÖKGÖZ MAHALLESİ",
      "HAYRETTİN MAHALLESİ",
      "HÜSEYİNLİ MAHALLESİ",
      "İSABALI MAHALLESİ",
      "KADIKÖY MAHALLESİ",
      "KARAPINAR MAHALLESİ",
      "KAZIMİYE MAHALLESİ",
      "KEMALİYE MAHALLESİ",
      "MEKECE MAHALLESİ"
    ],
    "03.05.2020": [
      "ORUÇLU MAHALLESİ",
      "ÖZBEK MAHALLESİ",
      "PAŞALAR MAHALLESİ",
      "PINARLI MAHALLESİ",
      "ŞAHMELEK MAHALLESİ",
      "ŞEYHVARMAZ MAHALLESİ",
      "TEŞVİKİYE MAHALLESİ",
      "TURGUTLU MAHALLESİ",
      "YENİCE MAHALLESİ"
    ],
  },
  "Sapanca": {
    "04.05.2020": [
      "AKÇAY MAHALLESİ",
      "BALKAYA MAHALLESİ",
      "CAMİCEDİT MAHALLESİ",
      "ÇAYİÇİ MAHALLESİ",
      "FEVZİYE MAHALLESİ",
      "GAZİPAŞA MAHALLESİ",
      "GÖL MAHALLESİ",
      "GÜLDİBİ MAHALLESİ",
      "HACIMERCAN MAHALLESİ"
    ],
    "05.05.2020": [
      "İKRAMİYE MAHALLESİ",
      "İLMİYE MAHALLESİ",
      "İSTANBULDERE MAHALLESİ",
      "KIRKPINAR HASANPAŞA MAHALLESİ",
      "KIRKPINAR SOĞUKSU MAHALLESİ",
      "KIRKPINAR TEPEBAŞI MAHALLESİ",
      "KURTKÖY DİBEKTAŞ MAHALLESİ",
      "KURTKÖY FATİH MAHALLESİ",
      "KURTKÖY YAVUZSELİM MAHALLESİ",
      "KURUÇEŞME MAHALLESİ",
      "MAHMUDİYE MAHALLESİ",
      "MEMNUNİYE MAHALLESİ"
    ],
    "06.05.2020": [
      "MURADİYE MAHALLESİ",
      "NAİLİYE MAHALLESİ",
      "RÜSTEMPAŞA MAHALLESİ",
      "ŞÜKRİYE MAHALLESİ",
      "UZUNKUM MAHALLESİ",
      "ÜNLÜCE MAHALLESİ",
      "YANIK MAHALLESİ",
      "YENİ MAHALLESİ"
    ],
  },
  "Serdivan": {
    "07.05.2020": [
      "ARABACIALANI MAHALLESİ",
      "ARALIK MAHALLESİ",
      "AŞAĞIDEREKÖY MAHALLESİ",
      "BAHÇELİEVLER MAHALLESİ"
    ],
    "08.05.2020": [
      "BEŞEVLER MAHALLESİ",
      "BEŞKÖPRÜ MAHALLESİ",
      "ÇUBUKLU MAHALLESİ",
      "DAĞYONCALI MAHALLESİ",
      "ESENTEPE MAHALLESİ",
      "HAMİTABAT MAHALLESİ"
    ],
    "09.05.2020": ["İSTİKLAL MAHALLESİ", "KAZIMPAŞA MAHALLESİ"],
    "10.05.2020": [
      "KEMALPAŞA MAHALLESİ",
      "KIZILCIKLI MAHALLESİ",
      "KÖPRÜBAŞI MAHALLESİ",
      "KURUÇEŞME MAHALLESİ"
    ],
    "11.05.2020": [
      "MEŞELİ MAHALLESİ",
      "ORTA MAHALLESİ",
      "OTUZİKİ EVLER MAHALLESİ",
      "REŞADİYE MAHALLESİ",
      "SELAHİYE MAHALLESİ",
      "UZUNKÖY MAHALLESİ",
      "VATAN MAHALLESİ",
      "YUKARIDEREKÖY MAHALLESİ"
    ],
  },
  "Söğütlü": {
    "11.05.2020": [
      "AKARCA MAHALLESİ",
      "AKÇAKAMIŞ MAHALLESİ",
      "AKGÖL MAHALLESİ",
      "BEŞDEĞİRMEN MAHALLESİ",
      "CAMİ CEDİT MAHALLESİ",
      "FINDIKLI MAHALLESİ",
      "GÜNDOĞAN MAHALLESİ",
      "HASANFAKI MAHALLESİ",
      "İMAMLAR MAHALLESİ"
    ],
    "12.05.2020": [
      "KANTAR MAHALLESİ",
      "KARATEKE MAHALLESİ",
      "KURUDİL MAHALLESİ",
      "KÜÇÜK SÖĞÜTLÜ MAHALLESİ",
      "LEVENT MAHALLESİ",
      "MAĞARA MAHALLESİ",
      "MAKSUDİYE MAHALLESİ",
      "ORTA MAHALLESİ",
      "RÜSTEMLER MAHALLESİ",
      "SIRAKÖY MAHALLESİ",
      "SOĞUCAK MAHALLESİ",
      "TOKMAKLIDERE MAHALLESİ",
      "TÜRKBEYLİKKIŞLA MAHALLESİ",
      "YENİKÖY MAHALLESİ"
    ],
  },
  "Taraklı": {
    "13.05.2020": [
      "AKÇAPINAR MAHALLESİ",
      "AKSU MAHALLESİ",
      "ALBALLAR MAHALLESİ",
      "AVDAN MAHALLESİ",
      "ÇAMTEPE MAHALLESİ",
      "DIŞDEDELER MAHALLESİ",
      "DUMAN MAHALLESİ",
      "ESENYURT MAHALLESİ",
      "HACIALİLER MAHALLESİ",
      "HACIMURAT MAHALLESİ",
      "HACIYAKUP MAHALLESİ",
      "HARKKÖY MAHALLESİ",
      "İÇDEDELER MAHALLESİ",
      "KEMALLER MAHALLESİ",
      "MAHDUMLAR MAHALLESİ",
      "PİRLER MAHALLESİ",
      "TUZLA MAHALLESİ",
      "UĞURLU MAHALLESİ",
      "ULUCAMİİ MAHALLESİ",
      "YENİDOĞAN MAHALLESİ",
      "YENİKÖY MAHALLESİ",
      "YUSUFBEY MAHALLESİ"
    ],
  },
};

const isimler = [
  "ELVEDA ÜREYEN",
  "BİŞAR OLGUN",
  "DOĞAN ATALI",
  "ABDULSEMET AKCAYOL",
  "ALİ HALUK DAL",
  "HACI KEMAL AY GÜNEY",
  "DERVİŞ ESENTUR",
  "OĞUZ KAĞAN ALICI",
  "SÜMEYYE FAYDACI",
  "BERCİS ASLANALP",
  "NURDAN KAYHAN",
  "ZEYNEP GÜNALAN",
  "ONUR TOPÇUOĞLU",
  "AKİF SEZGİNER",
  "UZAY AÇAR",
  "AYLA DEMİRBAĞ",
  "MERYEM KARAKOÇ",
  "MUHAMMED GÜRDEMİR",
  "BİRGÜL GEÇİCİ",
  "MİNA BORA",
  "EMİNE DİLEK SERGİN",
  "İSMAİL YAVUZ SOMAY",
  "VESI BAHÇEBAŞI",
  "ZEKİYE SEVAL HIZAL",
  "BATURAY ALIM",
  "DİDEM AYŞE KARADAĞ GEÇGEL",
  "BERAY ÇAPAR",
  "İBRAHİM TAYFUN MEMİŞOĞLU",
  "FAİK GENÇ TALAS",
  "DİLAY SUNGAR",
  "HACI HASAN KEKLİKÇİOĞLU",
  "KUTSEV DEMİRAY",
  "MUSTAFA NAFİZ SARIASLAN",
  "HANİFE TUĞÇE TUNCER",
  "DENİZALİ KARAİSMAİLOĞLU",
  "MEHMET HÜSEYİN MIZRAK",
  "EVREN DERYA",
  "HÜSEYİN CAHİT ORDULU ŞAHİN",
  "RANA DİLER",
  "ÜMMÜ GÜLSÜM SÜVERAN",
  "HÜSEYİN YAVUZ ALİ",
  "HÜLYA DUMAN",
  "HAŞİM ONUR SOBAY",
  "BİRSEN BOLAÇ",
  "KÜBRA ÖZYILMAZ",
  "SÜREYYA ÖNVERMEZ",
  "ABDURRAHMAN FUAT AVCI",
  "SANDRA BACAKSIZLAR",
  "SARE BOZPOLAT",
  "ÖZLEN YURTDAŞ",
  "SELİN BAKIR",
  "DİLBER ŞELİMAN",
  "FADIL BAKLALI",
  "FEVZİ FIRAT VATANSEVER",
  "ŞÜKRÜ EMERCE",
  "MAHİR KAHREMAN",
  "MAKBULE ÇİLESİZ",
  "ŞERİFE YASEMİN YAŞAN",
  "GÜLÇİN TUNÇER",
  "EMİRCAN ALPAYCI",
  "BAŞAK ÇELEN",
  "BARTU LÜLECİ",
  "FEYZAHAN YANMAZ",
  "SALİM ŞARE",
  "FEHMİ SEMİZ",
  "ENNUR MERDEN",
  "MUSA KANNECİ",
  "FATMA ESRA BAK",
  "SERHAT TOPAK",
  "LEZİZ DÖĞER",
  "MEHMET YILDIRIM ALTUNYAPRAK",
  "ABİDİN KALAY",
  "ECENAZ PULAT",
  "KALENDER ELDEM",
  "FUAT YARBİL",
  "ÖKKEŞ KURBAN",
  "ASLIHAN KÖSE",
  "ATAKAN DORLAY",
  "MELAHAT ORCAN",
  "NİHAT BERKAY BEKİŞ",
  "EMİRHAN DAM",
  "MUSTAFA ABDULLAH ZEYLİ",
  "BERKAN MEKİKOĞLU",
  "FEZA ÜNALDI",
  "TANJU TAŞKESEN YILMAZ",
  "DUHAN DEMİRKOL",
  "İLKNUR YANCAR",
  "BAHRİ KARA",
  "FATMA ESİN VARICI",
  "DİANA POTUK",
  "HATİCE ÖZGE ÖTEBAŞ",
  "CEMİL DAYANGAÇ",
  "ABDÜLSAMET KARATÜRK",
  "SEDEF YENİDÜNYA",
  "KÜRŞAT YAVUZ SEZGİN",
  "SAVAŞ TOPCUOĞLU",
  "DAMLA HALAÇ",
  "MAŞALLAH SAYAL",
  "NEBİL BUDAK",
  "ÖZGÜL ERGİNTÜRK ACAR",
  "ZEKİYE AKİ",
  "ÜNSAL YORGANCI",
  "UMUTCAN ULU",
  "SITKI ÖZKAN",
  "ŞERİF GÖNCÜ",
  "SERVET DEDEOĞLU",
  "MÜGE KARADAŞ",
  "ÇAĞLAR DÜZEN",
  "ZUHAL PERİŞAN",
  "DURAN ÇOLAKOĞLU",
  "SEVDE NUR SADIK",
  "VELİ ENES ÇİLDİR",
  "YASEMİN ÜRKMEZ",
  "FERDA OKUR",
  "FIRAT HALAÇOĞLU",
  "ZÜHTÜ BENER OFLUOĞLU",
  "ŞEYMA TAVİL",
  "AHMET SERKAN ÖZTAŞ",
  "KERİM TEN",
  "OSMAN TURGUT TOKAY",
  "FİKRİ CİCİOĞLU",
  "NECMETTİN SİPAHİ",
  "MUSTAFA AVŞAR",
  "HATİCE NAR",
  "KEZBAN YAKUT",
  "SEYRAN HEPKAYA",
  "İKRAM MARTİN",
  "TAHACAN KÖK",
  "REGAİP KARLI",
  "MUAMMER HAYRİ ZAİM",
  "MUHAMMET BAĞBUR CELİLOĞLU",
  "NECMİYE GÜL GÜZELKÜÇÜK",
  "JİHAT DEMİRALP",
  "ÖNER BOLAT",
  "SÜHEYLA AYÇA AYKOL",
  "MUSTAFA ASIM ÜLKÜ",
  "ABDULKERİM GÖKSU",
  "HÜSNÜ KEÇİK",
  "HURİYE GÖÇEN",
  "NİHAN SOYKAMER",
  "BÜŞRA ZOR",
  "EMİR KAAN KİŞİ",
  "GÜNEY ŞENCAN",
  "TAYLAN ZAFER",
  "DENİZ YARADILMIŞ",
  "MUSTAFA BARAN ARICI",
  "ŞULE GÜREL",
  "SAMİME ESEN",
  "BİRGÜN MÜJDECİ",
  "SEVCAN ÖNDER SİVİŞ",
  "VOLKAN ŞENER",
  "NECİP KARAKUŞ",
  "İNCİ GÖKŞEN",
  "SEYFULLAH ORHUN",
  "CEYHAN CILIZ BASHEER",
  "FATMA DUYGU GÖÇER",
  "GÜLSEREN TÜRKAL",
  "MEHMET BÜYÜKCENGİZ",
  "HATİCE NİLDEN TANTEKİN",
  "FATMA KAZBEK",
  "SEVGÜL TUTAR",
  "YUSUF ZİYA KARAER",
  "ASLIHAN ESRA İLBAŞ",
  "ABDULHALİM TURP",
  "TAYFUN ELLİALTI",
  "SEMA KARAPINAR",
  "ÖVGÜ ANIL KOLSUZ",
  "BUĞRA YETİZ",
  "ERDALCAN UYĞUN",
  "MAİDE TOPRAK",
  "EFKAN KÖYSÜREN",
  "NİZAMETTİN AYVA",
  "EMCED TURANOĞLU",
  "İNANÇ YURDAKUL",
  "HASAN CIVAN KALYENCİ",
  "CUMA BAZİKİ",
  "TANER AKTEMUR",
  "METE ÇAKIROGLU",
  "NURİ ANIL ÖCALAN",
  "MAHSUM ALMACIOĞLU",
  "HAMZA ÖZKÖSE",
  "TUFAN AKIN ELÇİÇEK",
  "BEDRİ İLKAY",
  "CEMİLE BAYAV",
  "EREM KÜFECİLER",
  "TURGAY TUZCU",
  "METEHAN YAPICI",
  "EYYÜP EMİNOĞLU",
  "YONCA ÇALIŞKANOĞLU",
  "SEÇKİN MYAMLKHAGVA",
  "FİLİZ GÜNDOĞDU",
  "TUĞRA YÜCE",
  "MURAT SÖZERİ",
  "SEZGİN KAVLAK",
  "RIDVAN KUŞAKÇI",
  "AYCAN ÖZDEN KÜÇÜKALİ",
  "FARUK ERCAN",
  "ELİFCAN PAZIR",
  "DOĞA ÜNÜVAR",
  "YILMAZ YACI",
  "FATMA EFSUN BÜYÜKBAŞ",
  "FUNDA ÖZLEM KULAK GEDİK",
  "YAŞAR BARBAROS KANGÜLEÇ",
  "BURÇİN AYKAN",
  "NARİN ÖZDEMİR AKDUR",
  "PEYVENT KAYA",
  "HAYRUNNİSA ALÇIKAYA",
  "NURBANU ERKÖSE",
  "ANDAÇ DARAMAN",
  "GAMZE YANNİ",
  "ÜLKÜHAN DURUK",
  "SEHER KÜRKÇÜOĞLU",
  "AHMET YÜCEDAĞ",
  "RAMAZAN FERHAD KUŞTİMUR",
  "MUAMMER BOĞA",
  "IRMAK TOKAR",
  "CEM ÖZGÜR ŞİRİN",
  "BİLGE KAYADİBİ",
  "AHMET FURKAN AKSEL",
  "EGE ÇÖLBAY",
  "VEYSİ AZAK",
  "JALE DAYANÇ",
  "FATİH AVNİ GAYRETLİ",
  "SIDDIKA TAN",
  "CANAN ÇETE",
  "GÜVEN DÜZCE",
  "SERAP TAYFUR",
  "AYŞE PINAR KURUOĞLU",
  "CEBBAR BULUÇ",
  "YAŞAR CIDIK"
];

const birimler = [
  "Dağıtım Personeli",
  "Paketleme Personeli",
  "Maske Personeli",
  "Dezenfektan Personeli"
];
