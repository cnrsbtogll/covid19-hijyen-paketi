import 'package:covid19_hijyen_paketi/neighborhood_detail.dart';
import 'package:flutter/material.dart';
import 'constants.dart';

class DistributionPlan extends StatefulWidget {
  @override
  _DistributionPlanState createState() => _DistributionPlanState();
}

class _DistributionPlanState extends State<DistributionPlan> {
  String district = "";
  String neighborhood = "";

  void changeDistrict(String e) {
    setState(() {
      district = e == district ? "" : e;
    });
  }

  void changeNeighborhood(String e) {
    setState(() {
      neighborhood = e == neighborhood ? "" : e;
    });
    navigateNeighborhoodDetail();
  }

  void navigateNeighborhoodDetail() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => NeighborhoodDetail(
          district: district,
          neighborhood: neighborhood,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.chevron_left, color: Colors.black),
                  onPressed: Navigator.of(context).pop,
                ),
              ),
              Image.asset(
                "assets/sakarya_bb.png",
                width: size.width * .3,
                height: size.width * .3,
              ),
              SizedBox(height: 16),
              Text(
                "Dağıtım Planı",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  letterSpacing: 1,
                ),
              ),
              SizedBox(height: 16),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: double.infinity,
                        width: double.infinity,
                        child: ListView(
                          children: [
                            Text(
                              "İlçe Seçiniz",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                            ...ilceListesi
                                .map(
                                  (e) => InkWell(
                                    onTap: () => changeDistrict(e),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        e,
                                        style: TextStyle(
                                          color: e == district
                                              ? Colors.blue
                                              : Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                                .toList()
                          ],
                        ),
                      ),
                    ),
                    if (district != "")
                      Expanded(
                        child: Container(
                          height: double.infinity,
                          width: double.infinity,
                          child: ListView(
                            children: [
                              Text(
                                "Mahalle Seçiniz",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                ),
                              ),
                              ...mahalleListesi[district]
                                  .map(
                                    (e) => InkWell(
                                      onTap: () => changeNeighborhood(e),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          e,
                                          style: TextStyle(
                                            color: e == neighborhood
                                                ? Colors.blue
                                                : Colors.grey,
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                  .toList()
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
