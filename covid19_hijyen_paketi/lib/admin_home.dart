import 'package:flutter/material.dart';

class AdminHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Image.asset(
                  "assets/sakarya_bb.png",
                  width: size.width * .3,
                  height: size.width * .3,
                ),
                SizedBox(height: 16),
                Text(
                  "Her eve hijyen paketi",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    letterSpacing: 1,
                  ),
                ),
                SizedBox(height: 48),
                renderOptionButton(
                  context,
                  "Stok durumu",
                  navigateTo: "/stock",
                ),
                Divider(height: 8, thickness: 1, color: Colors.grey),
                renderOptionButton(
                  context,
                  "Malzeme Listesi",
                  navigateTo: '/productlist',
                ),
                Divider(height: 8, thickness: 1, color: Colors.grey),
                renderOptionButton(
                  context,
                  "Personel Listesi",
                  navigateTo: "/employees",
                ),
                Divider(height: 8, thickness: 1, color: Colors.grey),
                renderOptionButton(
                  context,
                  "Dağıtım Planı",
                  navigateTo: "/distribution",
                ),
                Divider(height: 8, thickness: 1, color: Colors.grey),
                renderOptionButton(
                  context,
                  "Dağıtım Tarihleri",
                  navigateTo: "/dates",
                ),
                Divider(height: 8, thickness: 1, color: Colors.grey),
                renderOptionButton(
                  context,
                  "Dağıtımı Tamamlanmış Yerler",
                  navigateTo: "/completed",
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget renderOptionButton(BuildContext context, String title,
    {String navigateTo = "/admin"}) {
  return InkWell(
    onTap: () => Navigator.of(context).pushNamed(navigateTo),
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    child: Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                color: Colors.black54,
                fontSize: 14,
                fontWeight: FontWeight.w600,
                letterSpacing: 1,
              ),
            ),
          ),
          Icon(Icons.chevron_right, size: 24, color: Colors.black54),
        ],
      ),
    ),
  );
}
