import 'package:cloud_firestore/cloud_firestore.dart';
import 'constants.dart';
import 'dart:math';

final firestore = FirebaseFirestore.instance;

Future<void> fill_dummy() async {
  var rng = Random();
  for (String ilce in mahalleListesi.keys) {
    for (String mahalle in mahalleListesi[ilce]) {
      String refPath = ilce + "_" + mahalle;
      DocumentReference ref = firestore.collection("mahalleler").doc(refPath);
      int dagitilacak = rng.nextInt(80) + 20;
      int dagitilan = rng.nextInt(dagitilacak);
      await ref.set({
        "dağıtılacakPaket": dagitilacak,
        "dağıtılanPaket": dagitilan,
      });
      print(refPath);
    }
  }
}

Future<void> fill_employee() async {
  int duty = 0;
  CollectionReference ref = firestore.collection("personeller");
  for (int i = 0; i < 230; i++) {
    if (i < 50)
      duty = 0;
    else if (i < 150)
      duty = 1;
    else if (i < 200)
      duty = 2;
    else
      duty = 3;
    String dutyName = birimler[duty];
    String personName = isimler[i];
    await ref.add({
      "isim": personName,
      "maas": 3000,
      "birim": dutyName,
    });
  }
}
