import 'package:covid19_hijyen_paketi/employee_detail.dart';
import 'package:covid19_hijyen_paketi/product_detail.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final firestore = FirebaseFirestore.instance;

class EmployeeList extends StatefulWidget {
  @override
  _EmployeeListState createState() => _EmployeeListState();
}

class _EmployeeListState extends State<EmployeeList> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: null,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.chevron_left, color: Colors.black),
                  onPressed: Navigator.of(context).pop,
                ),
              ),
              Image.asset(
                "assets/sakarya_bb.png",
                width: size.width * .3,
                height: size.width * .3,
              ),
              SizedBox(height: 16),
              Text(
                "Personel Listesi",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  letterSpacing: 1,
                ),
              ),
              Text(
                "Düzenlemek istediğiniz personele tıklayınız",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black45,
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                  letterSpacing: 1,
                ),
              ),
              SizedBox(height: 32),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      "İsim",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        letterSpacing: 2,
                        color: Colors.blue[900],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "Görev",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        letterSpacing: 2,
                        color: Colors.blue[900],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "Ödeme",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        letterSpacing: 2,
                        color: Colors.blue[900],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              Expanded(child: EmployeeListStream()),
            ],
          ),
        ),
      ),
    );
  }
}

class EmployeeListStream extends StatelessWidget {
  void navigateDetail(BuildContext context, String id) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => EmployeeDetail(employeeId: id),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Query queryRef = firestore.collection("personeller").orderBy("birim");
    Stream<QuerySnapshot> stream = queryRef.snapshots();
    return StreamBuilder<QuerySnapshot>(
      stream: stream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());
        return ListView(
          children: snapshot.data.docs.map((e) {
            Map<String, dynamic> employee = e.data();

            return InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: () => navigateDetail(context, e.id),
              child: Column(
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(employee["isim"]),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        flex: 1,
                        child: Text(employee["birim"]),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        flex: 1,
                        child: Text(employee["maas"].toString() + " ₺"),
                      ),
                    ],
                  ),
                  Divider(height: 32, color: Colors.grey, thickness: 1),
                ],
              ),
            );
          }).toList(),
        );
      },
    );
  }
}
